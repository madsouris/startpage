# StartPage

[![Netlify Status](https://api.netlify.com/api/v1/badges/0ede76e7-1a17-43b9-b506-6e66decc8ead/deploy-status)](https://app.netlify.com/sites/practical-swartz-040674/deploys)
---
HTML startpage for browsers [Live here](https://startpagevuestyled.netlify.app)
![Sample](https://i.imgur.com/wEfBCrc.png)